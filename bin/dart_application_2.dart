import 'package:dart_application_2/dart_application_2.dart'
    as dart_application_2;
import 'dart:io';

void main() {
  print('Please input Message : ');
  String string = stdin.readLineSync()!.toUpperCase();
  List list = string.split(' ');
  var countList = list.toSet().toList();
  print('The Message is not duplicate word is : ');
  var stringList = countList.join(' ');
  print(stringList);
  print('Total of word in the Message is : ');
  print(countList.length);

  var countWord = Map();
  for (var element in list){
    if(!countWord.containsKey(element)){
      countWord[element] = 1;
    }else{
      countWord[element] += 1;
    }
  }
  print('Total of each word of Message is : ');
  print(countWord);
//finish
}
